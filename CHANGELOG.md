* pypimonitor 0.2.0 (unreleased)

    * Takes arguments (which packages to monitor) from a yaml file, or from `pkg=`, `cols=`, `user=`.
    * Now has two modes: command line and web server.
    * Name changed from "pypistats" to "pypimonitor".
    * Use chartsjs.org to draw line charts (was morrisjs before).
    * Package names are now case insensitive.

    -- Louis Paternault <spalax@gresille.org>

* pypimonitor 0.1.0 (2016-03-17)

    * First version. Everything will change in next version.

    -- Louis Paternault <spalax@gresille.org>
