PypiMonitor — Monitor your pypi packages
========================================

|sources| |pypi| |documentation| |license|

TODO Link to documentation `documentation <https://pypimonitor.readthedocs.io/en/latest>`__.

TODO Screenshot

What's new?
-----------

See `changelog
<https://git.framasoft.org/spalax/pypimonitor/blob/master/CHANGELOG.md>`_.

Download and install
--------------------

* From sources:

  * Download: https://pypi.python.org/pypi/pypimonitor
  * Install (in a `virtualenv`, if you do not want to mess with your distribution installation system)::

        python3 setup.py install

* From pip::

    pip install pypimonitor

* Quick and dirty Debian (and Ubuntu?) package

  This requires `stdeb <https://github.com/astraw/stdeb>`_ to be installed::

      python3 setup.py --command-packages=stdeb.command bdist_deb
      sudo dpkg -i deb_dist/pypimonitor-<VERSION>_all.deb

Documentation
-------------

* The compiled documentation is available on `readthedocs
  <http://pypimonitor.readthedocs.io>`_

* To compile it from source, download and run::

      cd doc && make html


.. |documentation| image:: http://readthedocs.org/projects/pypimonitor/badge
  :target: http://pypimonitor.readthedocs.io
.. |pypi| image:: https://img.shields.io/pypi/v/pypimonitor.svg
  :target: http://pypi.python.org/pypi/pypimonitor
.. |license| image:: https://img.shields.io/pypi/l/pypimonitor.svg
  :target: http://www.gnu.org/licenses/agpl-3.0.html
.. |sources| image:: https://img.shields.io/badge/sources-pypimonitor-brightgreen.svg
  :target: http://git.framasoft.org/spalax/pypimonitor
.. |build| image:: https://git.framasoft.org/spalax/pypimonitor/badges/master/build.svg
  :target: https://git.framasoft.org/spalax/pypimonitor/builds

