import argparse
import sys
import logging

import pypimonitor

LOGGER = logging.getLogger(pypimonitor.__name__)

def commandline_parser():
    """Return a command line parser."""

    parser = argparse.ArgumentParser(
        prog="pypimonitor",
        description="A year calendar producer.",
        )

    parser.add_argument(
        '--version',
        help='Show version',
        action='version',
        version='%(prog)s ' + pypimonitor.VERSION
        )

    parser.add_argument(
        '-u', '--user',
        help='A comma-separated list of users, whose packages are to be monitored.',
        action="append",
        nargs=1,
        default=[],
        )

    parser.add_argument(
        '-c', '--column',
        help='A comma-separated list of columns to show.',
        action="append",
        nargs=1,
        default=[],
        )

    parser.add_argument(
        '-p', '--package',
        help='A comma-separated list of packages to monitor.',
        action="append",
        nargs=1,
        default=[],
        )

    parser.add_argument(
        'yaml',
        help='Configuration file.',
        nargs="?",
        default=None,
        )

    return parser

def _flatten(list_of_lists):
    for llist in list_of_lists:
        for item in llist:
            yield from item.split(",")

if __name__ == "__main__":
    arguments = commandline_parser().parse_args()
    if ((arguments.yaml is not None) and (arguments.column or arguments.package or arguments.user)):
        LOGGER.error("Configuration file and --column, --package and --user arguments are incompatible.")
        sys.exit(1)
    if arguments.yaml is None:
        print(pypimonitor.Renderer.from_args(
            packages=_flatten(arguments.package),
            columns=_flatten(arguments.column),
            users=_flatten(arguments.user),
            ).render())
    else:
        print(pypimonitor.Renderer.from_yaml(arguments.yaml).render())
