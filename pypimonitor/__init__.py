#!/usr/bin/env python3

import collections
import colorsys
import datetime
import functools
import itertools
import jinja2
import json
import operator
import os
import pkg_resources
import random
import requests
import slumber
import sys
import urllib
import xmlrpc.client as xmlrpclib
import yaml

VERSION = "0.2.0-dev"

_client = xmlrpclib.ServerProxy('https://pypi.python.org/pypi')

class Counter:
    def __init__(self):
        self.value = 0

    def count(self):
        self.value += 1
        return self.value

def as_id(string):
    return "".join(list(filter(str.isalnum, string)))

def random_color(seed, saturation, value):
    old_seed = random.random()
    random.seed(seed)
    rgb = colorsys.hsv_to_rgb(random.random(), saturation, value)
    random.seed(old_seed)
    return ("#%02x%02x%02x" % tuple([int(value * 255) for value in rgb]))

def iter_user_packages(user):
    for _, package in _client.user_packages(user):
        try:
            yield package
        except urllib.error.HTTPError as error:
            sys.stderr.write("Ignoring user '{}': {}.".format(
                user,
                str(error),
                ))

class Renderer:

    def __init__(self, config):
        # Processing config
        self.config = config
        if "columns" not in self.config:
            self.config['columns'] = list(set(itertools.chain(*[
                    package.keys()
                    for package 
                    in self.config['packages'].values()
                ])))

        # Processing packages
        self.packages = {}
        for name in sorted(config['packages']):
            try:
                self.packages[name] = Package(name)
            except Exception as error:
                sys.stderr.write("Ignoring package '{}': {}.\n".format(name, error))
                continue

    @classmethod
    def from_yaml(cls, filename):
        with open(filename, 'r') as stream:
            return cls(yaml.safe_load(stream.read()))

    @classmethod
    def from_args(cls, packages=None, columns=None, users=None):
        config = {}

        # Processing packages
        if packages is None:
            packages = []
        packages = list(itertools.chain(*[item.split(",") for item in packages]))
        if users is not None:
            for user in users:
                packages.extend(iter_user_packages(user))
        packages = list(set(packages))
        config['packages'] = {pkg: {} for pkg in set(packages)}

        # Processing columns
        if columns is None:
            columns = []
        columns = list(itertools.chain(*[item.split(",") for item in columns]))
        if columns:
            config['columns'] = columns

        return cls(config)

    def __iter__(self):
        yield from self.packages

    def values(self):
        yield from self.packages.values()

    def __getitem__(self, key):
        return self.packages[key]

    @staticmethod
    def _get_environment():
        TEMPLATEDIR = os.path.join(
            pkg_resources.resource_filename(__name__, 'data'),
            'templates',
            )

        env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATEDIR))
        env.globals['counter'] = Counter().count
        env.globals['now'] = datetime.datetime.now()
        env.filters['datetime'] = operator.methodcaller('strftime', "%Y-%m-%d %H:%M:%S")
        env.filters['as_id'] = as_id
        env.filters['color'] = random_color
        return env

    def render(self):
        env = self._get_environment()

        # Check columns
        columns = []
        for column in self.config['columns']:
            env.get_template("columns/{}.html".format(column))
            columns.append(column)

        # Render
        template = env.get_template("index.html")
        return template.render(
            columns=columns,
            packages=self.packages,
            )


class Package:

    docs_api = {
        'readthedocs': slumber.API(base_url="http://readthedocs.io/api/v1/"),
        }

    def __init__(self, name):
        sys.stderr.write("Retrieving information about pypi package '{}'...\n".format(name))
        try:
            self.rawdata = json.loads(requests.get("http://pypi.python.org/pypi/{}/json".format(name)).text)
        except Exception as error:
            raise Exception("Error while getting data for package '{}': {}.".format(name, error))
        self.name = name
        self.info = self.rawdata["info"]
        self.releases = {}
        for version, files in self.rawdata['releases'].items():
            if not files:
                continue
            date = datetime.datetime.strptime(
                min((url['upload_time'] for url in files)),
                "%Y-%m-%dT%H:%M:%S",
                )
            self.releases[date] = {}
            self.releases[date]['downloads'] = sum((url['downloads'] for url in files))
            self.releases[date]['version'] = version
        if not self.releases:
            raise ValueError("Package '{}' does not exist or haven't any release.".format(name))

        downloads = 0
        for release in sorted(self.releases):
            self.releases[release]['previous'] = downloads
            downloads += self.releases[release]['downloads']

    def iter_dates(self):
        yield from self.releases.keys()

    @property
    def total_downloads(self):
        last = sorted(self.releases.keys())[-1]
        return self.releases[last]['downloads'] + self.releases[last]['previous']

    def get_documentation(self):
        # Readthedocs
        if self.docs_api['readthedocs'].project.get(slug=self.name.lower())['objects']:
            return (
                'readthedocs', 
                None,
                )

        # Pypi docs
        if self.info['docs_url'] is not None:
            return ("pypi", None)

        # No documentation
        return (None, None)
