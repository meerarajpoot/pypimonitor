from pypimonitor.column.jinja2 import Jinja2

class Readthedocs(Jinja2):
    keyword = 'readthedocs'
