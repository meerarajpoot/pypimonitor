# TODO Copyright

import jinja2

class Column:
    keyword = None
    default = {}

    def __init__(self, config):
        self.config = config

    @jinja2.contextfilter
    def __call__(self, context, package, options):
        filled = default.copy()
        if ('default' in config) and (self.keyword in config['default']):
            filled.update(self.config['default'][self.keyword])
        filled.update(options)
        self.render(context, package, options)

    def render(self, context, package, options):
        raise NotImplementedError()
