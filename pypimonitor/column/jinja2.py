from pypimonitor.column import Column

class Jinja2(Column):

    def render(self, context, package, options):
        TODO("""Make available:
        - package: the package name (as the key of the yaml dict)
        - pypi: the package information (as get from pypi)
        - column: the column options
        - config: the whole yaml file
        """)
        return context.environment.get_template(os.path.join("columns", self.keyword)).render(context)
